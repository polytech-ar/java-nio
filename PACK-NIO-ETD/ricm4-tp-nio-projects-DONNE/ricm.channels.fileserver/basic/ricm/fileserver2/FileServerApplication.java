package ricm.fileserver2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import ricm.channels.IBroker;
import ricm.channels.IBrokerListener;
import ricm.channels.IChannel;
import ricm.channels.IChannelListener;
import ricm.fileserver2.fileautomata.FileReader;

/*
 * Basic FileServer implementation The file is entirely read in memory before
 * sending it to client
 */

public class FileServerApplication implements IBrokerListener, IChannelListener {
	
	IBroker engine;
	String  folder;
	int     port;
	String  filename;
	
	FileReader fr; // The automaton to read the local file
	int        nsend = 0; // Number sent bytes
	
	
	void panic(String msg, Exception ex) {
		ex.printStackTrace(System.err);
		System.err.println("PANIC: " + msg);
		System.exit(-1);
	}
	
	byte[] intToByteArray(int i) {
		return ByteBuffer.allocate(4).putInt(i).array();
	}
	
	public FileServerApplication(IBroker engine, String folder, int port) throws Exception {
		this.port   = port;
		this.engine = engine;
		this.folder = folder;
		
		if (!folder.endsWith(File.separator))
			this.folder = folder + File.separator;
		
		this.fr = new FileReader(this.folder);
		this.engine.setListener(this);
		
		if (!this.engine.accept(port)) {
			System.err.println("Refused accept on " + port);
			System.exit(-1);
		}
		
	}
	
	byte[] readFile() throws IOException {
		return fr.read();
	}
	
	/**
	 * Callback invoked when a message has been received. The message is whole, all
	 * bytes have been accumulated.
	 * 
	 * Returns an error code if the request failed: -1: could not parse the request
	 * -2: file does not exist -3: unexpected error
	 * 
	 * @param channel
	 * @param bytes
	 */
	public void received(IChannel channel, byte[] request) {
		try {
			
			try {
				
				if (filename == null) { // If is the first message
					
					try { // We get the filename
						filename = new String(request, Charset.forName("UTF-8")); // dis.readUTF();
						fr.setFile(filename);
						System.out.println("FileServer - Receive request for downloading: " + filename);
						
						// We send the file' length
						byte[] length = intToByteArray(fr.length());
						channel.send(length);
					} catch (Exception ex) {
						ex.printStackTrace();
						channel.send(intToByteArray(-1)); // could not parse the request
						return;
					}
					
				}
				
				// We read the file step by step and send each read chunk 
				byte[] bytes;
				bytes = readFile();
				
				if (bytes == null)
					channel.send(intToByteArray(-2)); // requested file does not exist
				else {
					channel.send(bytes);
					nsend += bytes.length;
				}
				
			} catch (Exception ex) {
				ex.printStackTrace(System.err);
				channel.send(intToByteArray(-3));
			} finally {
				
				if (nsend >= fr.length()) { 
					// Case when the entire file has been read and sent
					nsend = 0;
					filename = null;
					
					// We send an empty byte array to close the client' connection
					channel.send(new byte[1]);
				}
				
			}
			
		} catch (Exception ex) {
			panic("unexpected exception", ex);
		}
		
	}
	
	@Override
	public void connected(IChannel c) {
		System.out.println("Unexpected connected");
		System.exit(-1);
	}
	
	@Override
	public void accepted(IChannel c) {
		System.out.println("Accepted");
		c.setListener(this);
	}
	
	@Override
	public void refused(String host, int port) {
		System.out.println("Refused " + host + ":" + port);
		System.exit(-1);
	}
	
	@Override
	public void closed(IChannel c, Exception e) {
		System.out.println("Client closed channel");
	}
}
