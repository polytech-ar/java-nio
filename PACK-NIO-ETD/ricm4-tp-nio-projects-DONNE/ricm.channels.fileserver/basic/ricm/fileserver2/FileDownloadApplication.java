package ricm.fileserver2;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import ricm.channels.IBroker;
import ricm.channels.IBrokerListener;
import ricm.channels.IChannel;
import ricm.channels.IChannelListener;

public class FileDownloadApplication implements IBrokerListener, IChannelListener {
	
	String  server;
	int     port;
	String  filename;
	IBroker engine;
	boolean isText;
	
	int fileLength = -1; // Length of the file to download
	int nreceived  = 0; // Number of received bytes 
	
	
	int byteArrayToInt(byte[] bytes) {
		return ByteBuffer.wrap(bytes).getInt();
	}
	
	public FileDownloadApplication(IBroker engine) {
		this.engine = engine;
		this.engine.setListener(this);
	}
	
	public void download(String hostname, int port, String filename, boolean isText) throws Exception {
		this.filename = filename;
		this.isText   = isText;
		
		if (!this.engine.connect(hostname, port)) {
			System.err.println("Refused connect on " + port);
			System.exit(-1);
		}
		
	}
	
	@Override
	public void connected(IChannel c) {
		System.out.println("Connected");
		System.out.println("  downloading " + filename);
		c.setListener(this);
		
		try {
			c.send(filename.getBytes(Charset.forName("UTF-8")));
		} catch (Exception ex) {
			ex.printStackTrace(System.err);
			System.exit(-1);
		}
		
	}
	
	@Override
	public void accepted(IChannel c) {
		System.out.println("Unexpected accepted connection");
		System.exit(-1);
	}
	
	@Override
	public void refused(String host, int port) {
		System.out.println("Refused " + host + ":" + port);
		System.exit(-1);
	}
	
	@Override
	public void received(IChannel c, byte[] reply) {
		
		if (fileLength < 0) { // We get the length of the file to download
			fileLength = byteArrayToInt(reply);
		} else {
			
			if (nreceived < 1) { // If we are at the beginning of the downloading
				System.out.println("===============================================================");
				System.out.println("Received:");
				System.out.println("===============================================================");
			}
			
			try {
				
				if (isText) { // We get the received byte.
					String txt = new String(reply, "UTF-8");
					System.out.print(txt);
					
					if(nreceived < fileLength) {
						// If the file still uncomplete, we send an empty byte array to get the rest of the file 
						c.send(new byte[1]);
						nreceived += reply.length;
					}else {
						// If the file is complete, we print an empty line for a pretty display
						System.out.println("");
					}
				}
				
			} catch (Exception ex) {
				System.out.println(" failed parsing the received message");
				ex.printStackTrace();
			}
			
			if(nreceived >= fileLength) {
				// If the file is complete, print the completion message and close the connection
				System.out.println("===============================================================");
				System.out.println("\n\nBye");
				c.close();
				System.exit(0);
			}
		}
		
	}
	
	@Override
	public void closed(IChannel c, Exception e) {
		System.out.println("Unexpected closed channel");
		if (e != null)
			e.printStackTrace();
		System.exit(-1);
	}
	
}
