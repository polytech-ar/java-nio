package ricm.fileserver2.fileautomata;


public enum FileState {
	IDLE,
	WRITE, // Fore FileWrite automata to save the file
	READ
}
