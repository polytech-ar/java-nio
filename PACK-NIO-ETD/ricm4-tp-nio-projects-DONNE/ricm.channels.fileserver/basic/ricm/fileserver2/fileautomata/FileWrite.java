package ricm.fileserver2.fileautomata;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileWrite {
	
	private static final int CHUNK_SIZE = 512;
	
	private FileState        state;
	private File             file;
	private FileOutputStream fos;
	
	private int    nwrite;
	private int    fileLength;
	private byte[] buff;
	
	
	public FileWrite() {
		this.state  = FileState.IDLE;
		this.nwrite = 0;
	}
	
	public void save(String filename, int expectedLength) {
		this.file = new File(filename);
		if (file.exists())
			throw new IllegalArgumentException(filename + " already exists");
		this.fileLength = expectedLength;
	}
	
	public void write(byte[] bytes) throws IOException {
		
		switch (state) {
			case IDLE:
				nwrite = 0;
				
				if (fileLength > 0) {
					fos   = new FileOutputStream(file);
					state = FileState.WRITE;
				}
			case WRITE:
				if(bytes == null)
					throw new IllegalArgumentException("bytes empty");
				
				fos.write(bytes);
				nwrite += bytes.length;
				
				if(finished()) {
					state = FileState.IDLE;
				}
				
				
				break;
			default:
				throw new IllegalStateException("Invalid state in FileWriter automaton");
		}
		
	}
	
	public boolean finished() {
		return nwrite >= fileLength;
	}
	
}
