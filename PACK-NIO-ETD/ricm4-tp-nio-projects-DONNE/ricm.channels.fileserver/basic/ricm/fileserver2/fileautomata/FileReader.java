package ricm.fileserver2.fileautomata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;

public class FileReader {
	
	private static final int CHUNK_SIZE = 512;
	
	private FileState       state;
	private String          folder;
	private File            file;
	private FileInputStream fis;
	
	private int    nread;
	private int    fileLength;
	private byte[] buff;
	
	
	public FileReader(String folder) {
		this.folder = folder;
		this.state  = FileState.IDLE;
	}
	
	public byte[] read() throws IOException {
		
		switch (state) {
			case IDLE:
				buff = new byte[CHUNK_SIZE];
				nread = 0;
				
				if (fileLength > 0) {
					fis   = new FileInputStream(file);
					state = FileState.READ;
				}
			case READ:
				int n = fis.read(buff, 0, CHUNK_SIZE);
				
				if(n == -1) {
					int diff = fileLength - nread;
					buff = Arrays.copyOfRange(buff, 0, diff);
				}else {
					if(n < CHUNK_SIZE) {
						buff = Arrays.copyOfRange(buff, 0, n);
					}
					
					nread += n;
				}
				
				if (finished()) {
					state = FileState.IDLE;
					fis.close();
					fileLength = 0;
				}
				
				return buff;
			default:
				throw new IllegalStateException("Invalid state in FileReader automata");
		}
		
	}
	
	public boolean finished() {
		return nread == fileLength;
	}
	
	public void setFile(String filename) {
		file = new File(folder + filename);
		if (!file.exists() || !file.isFile())
			throw new IllegalArgumentException(folder + filename + " doesn't exist or is not a file");
		
		fileLength = (int) file.length();
	}
	
	public int length() {
		return fileLength;
	}
	
}
