package ricm.channels.nio;

public enum State {
	IDLE,
	READ_LENGTH,
	READ_CONTENT,
	WRITE_LENGTH,
	WRITE_CONTENT;
}
