package ricm.channels.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.Arrays;
import java.util.LinkedList;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

public class Writer {
	
	private State              state;
	private SocketChannel      sc;
	private SelectionKey       key;
	private ByteBuffer         dataBuf;
	private ByteBuffer         lenBuf;
	private LinkedList<byte[]> msgs;
	
	
	public Writer(SelectionKey key) {
		this.key    = key;
		this.sc     = (SocketChannel) key.channel();
		this.lenBuf = ByteBuffer.allocate(4);
		this.state  = State.IDLE;
		this.msgs   = new LinkedList<byte[]>();
	}
	
	public void send(byte data[]) {
		this.send(data, 0, data.length);
	}
	
	public void send(byte data[], int offset, int count) {
		this.msgs.add(Arrays.copyOfRange(data, offset, offset + count));
		this.key.interestOps(SelectionKey.OP_WRITE | SelectionKey.OP_READ);
	}
	
	public void handleWrite() throws IOException {
		
		switch (this.state) {
			case IDLE:
				if (this.msgs.size() > 0) {
					byte msg[] = this.msgs.pop();
					
					this.lenBuf.position(0);
					this.lenBuf.putInt(msg.length);
					this.lenBuf.rewind();
					this.dataBuf = ByteBuffer.wrap(msg);
					
					this.state = State.WRITE_LENGTH;
				} else
					break;
			case WRITE_LENGTH:
				this.sc.write(this.lenBuf);
				if (this.lenBuf.remaining() == 0)
					this.state = State.WRITE_CONTENT;
				else
					break;
			case WRITE_CONTENT:
				this.sc.write(this.dataBuf);
				
				if (this.dataBuf.remaining() == 0)
					this.state = State.IDLE;
				
				if (this.msgs.size() == 0)
					this.key.interestOps(SelectionKey.OP_READ);
				
				break;
			default:
				throw new IllegalStateException("Invalid state in writer automata");
		}
		
	}
	
	public void setKey(SelectionKey key) {
		
		if (key != null) {
			this.key = key;
			this.sc  = (SocketChannel) key.channel();
		}
		
	}
}
