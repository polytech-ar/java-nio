package ricm.channels.nio;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.channels.spi.AbstractSelectableChannel;
import java.nio.channels.spi.SelectorProvider;
import java.util.Iterator;

import ricm.channels.IBroker;
import ricm.channels.IBrokerListener;
import ricm.channels.IChannel;
import ricm.channels.IChannelListener;

public class Broker implements IBroker, Runnable {
	
	private IBrokerListener listener;
	private IChannel        channel;
	
	private int      port;
	private String   host;
	private Selector selector;
	
	// Client
	private SelectionKey  scKey;
	private SocketChannel sc;
	
	// Server
	private SelectionKey        sscKey;
	private ServerSocketChannel ssc;
	
	
	public Broker() throws IOException {
		this.selector = SelectorProvider.provider().openSelector();
	}
	
	@Override
	public void setListener(IBrokerListener l) {
		this.listener = l;
	}
	
	@Override
	public boolean connect(String host, int port) {
		InetAddress addr;
		
		this.port = port;
		this.host = host;
		
		try {
			this.sc = SocketChannel.open();
			this.sc.configureBlocking(false);
			
			this.scKey = this.sc.register(selector, SelectionKey.OP_CONNECT);
			
			addr = InetAddress.getByName(host);
			
			this.sc.connect(new InetSocketAddress(addr, port));
			
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public boolean accept(int port) {
		
		try {
			this.ssc = ServerSocketChannel.open();
			this.ssc.configureBlocking(false);
			
			InetAddress       hostAddress = InetAddress.getByName("localhost");
			InetSocketAddress isa         = new InetSocketAddress(hostAddress, port);
			this.ssc.socket().bind(isa);
			
			// be notified when connection requests arrive
			this.sscKey = this.ssc.register(selector, SelectionKey.OP_ACCEPT);
			
		} catch (IOException e) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public void run() {
		System.out.println("Broker run");
		while (true) {
			
			try {
				this.selector.select();
				
				Iterator<?> selectedKeys = this.selector.selectedKeys().iterator();
				
				while (selectedKeys.hasNext()) {
					SelectionKey key = (SelectionKey) selectedKeys.next();
					// process key's events
					if (key.isValid() && key.isAcceptable())
						handleAccept(key);
					if (key.isValid() && key.isReadable())
						handleRead(key);
					if (key.isValid() && key.isWritable())
						handleWrite(key);
					if (key.isValid() && key.isConnectable())
						handleConnect(key);
					// remove the key from the selected-key set
					selectedKeys.remove();
				}
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	private void handleAccept(SelectionKey key) throws IOException {
		assert (this.sscKey == key);
		assert (ssc == key.channel());
		SocketChannel scTmp;
		
		// do the actual accept on the server-socket channel
		scTmp = ssc.accept();
		scTmp.configureBlocking(false);
		
		// register the read interest for the new socket channel
		// in order to know when there are bytes to read
		SelectionKey k = scTmp.register(this.selector, SelectionKey.OP_READ);
		
		this.channel = new Channel(k);
		this.channel.setListener((IChannelListener) this.listener);
		this.listener.accepted(this.channel);
	}
	
	private void handleConnect(SelectionKey key) {
		assert (this.scKey == key);
		assert (sc == key.channel());
		
		try {
			sc.finishConnect();
			key.interestOps(SelectionKey.OP_READ);
		} catch (IOException e) {
			this.listener.refused(this.host, this.port);
		} finally {
			this.channel = new Channel(this.scKey);
			this.channel.setListener((IChannelListener) this.listener);
			this.listener.connected(this.channel);
		}
		
	}
	
	private void handleWrite(SelectionKey key) throws IOException {
		((Channel) this.channel).handleWrite();
	}
	
	private void handleRead(SelectionKey key) throws IOException {
		((Channel) this.channel).handleRead();
	}
	
}
