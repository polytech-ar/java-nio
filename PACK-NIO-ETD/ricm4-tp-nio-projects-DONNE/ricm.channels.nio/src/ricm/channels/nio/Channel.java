package ricm.channels.nio;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

import ricm.channels.IChannel;
import ricm.channels.IChannelListener;
import sun.security.util.Length;

public class Channel implements IChannel {
	
	private IChannelListener listener;
	
	private Writer writer;
	private Reader reader;
	
	private boolean closed = false;
	
	
	public Channel(SelectionKey k) {
		this.writer = new Writer(k);
		this.reader = new Reader((SocketChannel) k.channel());
	}
	
	@Override
	public void setListener(IChannelListener l) {
		this.listener = l;
	}
	
	@Override
	public void send(byte[] bytes, int offset, int count) {
		this.writer.send(bytes, offset, count);
		
	}
	
	@Override
	public void send(byte[] bytes) {
		this.send(bytes, 0, bytes.length);
	}
	
	@Override
	public void close() {
		this.closed = true;
	}
	
	@Override
	public boolean closed() {
		return this.closed;
	}
	
	public void handleWrite() {
		try {
			this.writer.handleWrite();
		} catch (IOException e) {
			this.listener.closed(this, e);
		}
	}
	
	public void handleRead(){
		try {
			byte data[] = this.reader.handleRead();
			if(data != null)
				this.listener.received(this, data);
		} catch (IOException e) {
			this.listener.closed(this, e);
		}
	}
	
}
