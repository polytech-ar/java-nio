package ricm.distsys.nio.babystep2;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Reader {
	
	private static final int LEN_BUFF_SIZE = 4;
	
	private int len;
	
	private State         state;
	private SocketChannel sc;
	private ByteBuffer    lenBuf;
	private ByteBuffer    dataBuf;
	
	public Reader(SocketChannel sc) {
		this.sc     = sc;
		this.lenBuf = ByteBuffer.allocate(LEN_BUFF_SIZE);
		this.state  = State.READ_LENGTH;
	}
	
	public byte[] handleRead() throws IOException {
		switch (this.state) {
			case READ_LENGTH:
				this.sc.read(lenBuf);
				if (this.lenBuf.remaining() == 0) {
					lenBuf.position(0);
					this.len     = lenBuf.getInt();
					this.dataBuf = ByteBuffer.allocate(len);
					this.lenBuf.rewind();
					state = State.READ_CONTENT;
				} else
					break;
			case READ_CONTENT:
				this.sc.read(this.dataBuf);
				if (this.dataBuf.remaining() == 0) {
					this.state = State.READ_LENGTH;
					return this.dataBuf.array();
				}
				break;
			default:
				throw new IllegalStateException("Invalid state in reader automata");
		}
		
		return null;
	}
}
