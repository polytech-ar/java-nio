package ricm.distsys.nio.babystep3;

public enum State {
	IDLE,
	READ_LENGTH,
	READ_CONTENT,
	WRITE_LENGTH,
	WRITE_CONTENT;
}
